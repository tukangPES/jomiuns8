import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./main-page/main-page.module').then(m => m.MainPageModule)
  },
  {
    path: 'dirWha',
    loadChildren: () => import('./dir-wha/dir-wha.module').then(m => m.ModuleDirWhaModule)
  },
  {
    path: 'tokpedHelper',
    loadChildren: () => import('./tokped-tool/tokped-tool.module').then(m => m.TokpedHelperModule)
  },
  {
    path: 'shopeeHelper',
    loadChildren: () => import('./shopee-search/shopee-search.module').then(m => m.ShopeeSearchModule)
  },
  {
    path: 'docMaintain',
    loadChildren: () => import('./doc-maintain/doc-maintain.module').then(m => m.DocMaintainModule)
  },
  {
    path: 'redisMaintain',
    loadChildren: () => import('./redis-maintain/redis-maintain.module').then(m => m.RedisMaintainModule)
  },
  {
    path: 'docMaintain2',
    loadChildren: () => import('./doc-maintain-jul/doc-maintain-jul.module').then(m => m.DocMaintainJulModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./xlogin/xlogin.module').then(m => m.XloginModule)
  },
  {
    path: 'envMaintain',
    loadChildren: () => import('./env-maintain/env-maintain.module').then(m => m.EnvMaintainModule)
  },
  {
    path: 'emoji',
    loadChildren: () => import('./emoji/emoji.module').then(m => m.EmojiModule)
  },
  {
    path: 'mapost',
    loadChildren: () => import('./mapost/mapost.module').then(m => m.MapostModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./xlogin/xlogin.module').then(m => m.XloginModule),
    data: {isLogOut: true}
  },
  {
    path: 'todo',
    loadChildren: () => import('./todo/todo.module').then(m => m.TodoModule),
    data: {isLogOut: true}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
