import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonChangerComponent } from './button-changer.component';

describe('ButtonChangerComponent', () => {
  let component: ButtonChangerComponent;
  let fixture: ComponentFixture<ButtonChangerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonChangerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonChangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
