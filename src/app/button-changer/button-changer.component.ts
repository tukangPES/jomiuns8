import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button-changer',
  templateUrl: './button-changer.component.html',
  styleUrls: ['./button-changer.component.css']
})
export class ButtonChangerComponent implements OnInit {

  @Input() label: string = '';
  @Input() owner: any = {};
  @Input() prop = '';
  @Input() value: any;
  xChecked: boolean = false;

  //event saat item ini clicked, baik checked maupun unchecked
  @Output() onClick: EventEmitter<ButtonChangerComponent> = new EventEmitter();

  //event saat item ini onInit
  @Output() xOnItemCreated: EventEmitter<ButtonChangerComponent> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.doRefresh();
    this.xOnItemCreated.emit(this);
  }

  public doRefresh(): void {
    var theProp: [] | null | undefined = this.owner[this.prop];
    if (theProp !== undefined) {
      console.log('theProp', theProp);
      console.log('this.value', this.value);

      if (theProp.find((theItem) => theItem == this.value) != null) {
        this.xChecked = true;
      }
    }
  }

  onClicked(theEvent: any) {
    let theProp = this.owner[this.prop];

    if (theEvent.checked) {
      if (theProp === undefined)
        theProp = [];
      console.log('theProp', theProp);
      console.log('value', this.value);
      theProp.push(this.value);
      this.owner[this.prop] = theProp;
    }
    else {
      this.owner[this.prop] = theProp.filter((theItem: any) => theItem !== this.value);
    }

    if (this.onClick != null && this.onClick !== undefined) {
      this.onClick.emit(this);
    }
  }
}
