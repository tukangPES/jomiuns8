import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { ButtonChangerComponent } from './button-changer.component';



@NgModule({
  declarations: [ButtonChangerComponent],
  imports: [
    CommonModule,
    CheckboxModule,
    FormsModule,
  ],
  exports:[
    ButtonChangerComponent
  ]
})
export class ButtonChangerModule { }
