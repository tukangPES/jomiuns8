import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirWhaComponent } from './dir-wha.component';

describe('DirWhaComponent', () => {
  let component: DirWhaComponent;
  let fixture: ComponentFixture<DirWhaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirWhaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirWhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
