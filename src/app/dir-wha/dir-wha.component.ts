import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { whaFeaturedPhone, whaPhone, whaPhoneGroup } from 'src/classes/interfaces';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';
import { redisGetObject } from '../main-page/main-page.component';

declare const jgp: any;

@Component({
  selector: 'app-dir-wha',
  templateUrl: './dir-wha.component.html',
  styleUrls: ['./dir-wha.component.css']
})
export class DirWhaComponent implements OnInit {

  myConfig: xConfiguration;
  lang = '';
  phNumber = '';
  nomorTelpPenting: whaPhone[] = [];
  groups: whaPhoneGroup[] = [];
  yeah: HTMLElement = document.querySelector('#theIcon');

  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient) {
  }

  async doPatch01() {
    redisGetObject(this.httpClient, 'DirWhaComponent').then(yeah => {
      this.yeah['href'] = yeah.icon;
      document.title = yeah.title;
    });
  }

  async ngOnInit(): Promise<void> {
    this.myConfig = await readConfig(this.httpClient);
    this.lang = this.cookieService.get('theLang');
    if (this.lang === '') {
      this.lang = 'id';
    }
    this.doPatch01();

    redisGetObject(this.httpClient, 'featuredPhoneNumber02').then(yeah => {
      this.nomorTelpPenting = yeah.phoneNumbers;
      this.groups = yeah.phoneGroups;
    });

  }

  doClear(): void {
    this.phNumber = '';
  }

  doKirim(something: string | null): void {
    let theNomor = this.phNumber;
    if (theNomor != null) {
      if (theNomor !== undefined) {
        if (theNomor[0] !== '0') {
          theNomor = '0' + theNomor;
        }
      }
    }

    if (something === undefined) {
      jgp('doDirectWha', theNomor);
    }
    else if (something == null) {
      jgp('doDirectWha', theNomor);
    }
    else {
      jgp('doDirectWha', something);
    }
  }

  onLangChange(newLang: string): void {
    this.lang = newLang;
    this.cookieService.set('theLang', newLang);
  }
}