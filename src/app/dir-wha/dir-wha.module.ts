import { AccordionModule } from 'primeng/accordion';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DirWhaComponent } from './dir-wha.component';
import { MyPhoneGroupComponent } from '../my-phone-group/my-phone-group.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { InputTextModule } from 'primeng/inputtext';

let rout: Routes = [
  {
    path: '',
    component: DirWhaComponent
  }
]

@NgModule({
  declarations: [DirWhaComponent, MyPhoneGroupComponent],
  imports: [
    CommonModule,
    FormsModule,
    AccordionModule,
    InputTextModule,
    ButtonModule,
    RouterModule.forChild(rout)
  ],
  providers: [CookieService],
})
export class ModuleDirWhaModule { }
