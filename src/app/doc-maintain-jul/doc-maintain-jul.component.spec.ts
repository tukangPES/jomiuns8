import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocMaintainJulComponent } from './doc-maintain-jul.component';

describe('DocMaintainJulComponent', () => {
  let component: DocMaintainJulComponent;
  let fixture: ComponentFixture<DocMaintainJulComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocMaintainJulComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocMaintainJulComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
