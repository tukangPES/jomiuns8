import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';
import { redisGetObject } from '../main-page/main-page.component';

@Component({
  selector: 'app-doc-maintain-jul',
  templateUrl: './doc-maintain-jul.component.html',
  styleUrls: ['./doc-maintain-jul.component.css']
})
export class DocMaintainJulComponent implements OnInit {

  leParam = { leKeyword: '' };
  browseAble: any[] = [];
  yeah: HTMLElement = document.querySelector('#theIcon');
  myConfig: xConfiguration;

  constructor(
    private httpClient: HttpClient,
    private xSpinner: NgxSpinnerService) {
  }

  async doPatch01() {
    redisGetObject(this.httpClient, 'DocMaintainJulComponent').then(aObj => {
      this.yeah['href'] = aObj.icon;
      document.title = aObj.title;
    })
  }

  async ngOnInit(): Promise<void> {
    this.myConfig = await readConfig(this.httpClient);
    this.doPatch01();
  }

  doNew(){
    window.open('docMaintain');
  }

  onEnter(aEvent) {
    console.log(aEvent);
    this.doSearch();
  }

  linkToDoc(aKnow) {
    const addParam = environment.production ? '' : '';
    const strURL = `${this.myConfig.apiURLdomain}/orm/doc/${aKnow.friendlyTitle}?${addParam}`;
    return strURL;
  }

  linkToEditDoc(aKnow) {
    const addParam = environment.production ? '' : '';
    const strURL = `docMaintain?key=${aKnow.friendlyTitle}`;
    return strURL;
  }

  doSearch() {
    const addParam = environment.production ? '' : '';
    const strURL = `${this.myConfig.apiURLdomain}/orm/doc/search?${addParam}`;
    const anObject = {
      keyword: `${this.leParam.leKeyword}`
    }
    console.log(`hitting ${anObject.keyword}`);
    this.xSpinner.show();
    this.httpClient.post(strURL, anObject, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .toPromise()
      .then((leResult: any) => {
        console.log(leResult);
        this.browseAble = leResult;
      }).catch(theErr => {
        console.log(theErr);

      }).finally(() => {
        this.xSpinner.hide();
      })

  }

}
