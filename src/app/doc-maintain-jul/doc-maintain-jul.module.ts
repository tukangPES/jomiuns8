import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocMaintainJulComponent } from './doc-maintain-jul.component';
import { RouterModule, Routes } from '@angular/router';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';


const leRout: Routes = [
  {
    path: '',
    component: DocMaintainJulComponent
  }
]

@NgModule({
  declarations: [DocMaintainJulComponent],
  imports: [
    CommonModule,
    InputTextModule,
    ButtonModule,
    FormsModule,
    RouterModule.forChild(leRout)
  ],
  exports:[
    DocMaintainJulComponent
  ]
})
export class DocMaintainJulModule { }
