import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocMaintainComponent } from './doc-maintain.component';

describe('DocMaintainComponent', () => {
  let component: DocMaintainComponent;
  let fixture: ComponentFixture<DocMaintainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocMaintainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocMaintainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
