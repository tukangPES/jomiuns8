import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import firebase from 'firebase/app';
import 'firebase/auth';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { enLocStor } from 'src/classes/enumss';
import { Guid } from "guid-typescript";
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';
import { redisGetObject } from '../main-page/main-page.component';
import { environment } from 'src/environments/environment';

declare let lastRange: any;

@Component({
  selector: 'app-doc-maintain',
  templateUrl: './doc-maintain.component.html',
  styleUrls: ['./doc-maintain.component.css']
})
export class DocMaintainComponent implements OnInit {

  @ViewChild('div01') theDiv01: any;
  yeah: HTMLElement = document.querySelector('#theIcon');

  membutuhkanPassword = false;
  thePassword = '';

  public docIdOrFriendlyTitle = '';

  private firebaseConfig = {
    apiKey: 'AIzaSyD08Wpbz2EnlsGbwMVYGamgBvnhhdZCOZo',
    authDomain: 'api-8234549410433384822-425177.firebaseapp.com',
    databaseURL: 'https://api-8234549410433384822-425177-default-rtdb.asia-southeast1.firebasedatabase.app/',
    storageBucket: 'api-8234549410433384822-425177.appspot.com'
  };

  aUser: any;
  theObject: any = {};
  patchTokenId = '';
  myConfig: xConfiguration;

  constructor(private httpClient: HttpClient,
    private xSpinner: NgxSpinnerService,
    private toastr: ToastrService,
    private viewRef: ViewContainerRef,
    private router: ActivatedRoute) {
    firebase.initializeApp(this.firebaseConfig);
  }

  async doPatch01() {
    redisGetObject(this.httpClient, 'DocMaintainComponent').then(aObj => {
      this.yeah['href'] = aObj.icon;
      document.title = aObj.title;
    });
  }

  onNew() {
    this.docIdOrFriendlyTitle = 'NEW';
    this.doLoad();
  }

  triggerFunction(theEvent) {
    if (theEvent.ctrlKey && theEvent.key === 'Enter') {
      this.onSave(this.theDiv01.nativeElement.innerHTML, this.theDiv01.nativeElement.innerText);
    }
  }

  doViewLink() {
    const addParam = environment.production ? '' : '';
    const strURL = `${this.myConfig.apiURLdomain}/orm/doc/${this.theObject.friendlyTitle}?${addParam}`;
    window.open(strURL);
  }

  async ngOnInit(): Promise<void> {
    this.myConfig = await readConfig(this.httpClient);
    firebase.auth().onAuthStateChanged((theUser) => {
      this.patchTokenId = localStorage.getItem(enLocStor.googleTokenId);
      this.aUser = theUser;
    });

    this.router.queryParamMap
      .subscribe((theResult) => {
        this.docIdOrFriendlyTitle = theResult.get('key');
        if (this.docIdOrFriendlyTitle === undefined || this.docIdOrFriendlyTitle == null) {
          this.docIdOrFriendlyTitle = 'NEW';
        }
        this.doLoad();
      });

    this.doPatch01();
  }

  doLoad(): void {
    if (this.docIdOrFriendlyTitle === 'NEW') {
      this.theObject = {
        knowledgeID: 'NEW',
        content: '',
        title: '',
        friendlyTitle: ''
      };
      this.theDiv01.nativeElement.innerHTML = '';
      return;
    }

    this.xSpinner.show();
    const addParam = environment.production ? '' : '';
    const strURL = `${this.myConfig.apiURLdomain}/orm/doc/${this.docIdOrFriendlyTitle}?raw=true&${addParam}`;
    this.httpClient.get(strURL, {
      headers:{
        'plainPassword': this.thePassword
      }
    })
      .toPromise()
      .then(result => {
        this.theObject = result;
        this.membutuhkanPassword = false;

        if (this.theObject.publicity != null &&
            this.theObject.publicity != 'public' &&
            this.theObject.publicity != 'private' )
            {
              this.theObject.publicity = 'password'
            }

      })
      .catch(err => {
        if (err.error.text == 'membutuhkan password' ||
            err.error.text == 'password salah')
        {
          this.toastr.show(err.error.text);
          this.membutuhkanPassword = true;
          return;
        }

        console.log(err);
        this.toastr.show(err);
      })
      .finally(() => {
        this.xSpinner.hide();
      })
  }

  async onDelete() {

    let theTokenId = ''
    if (this.patchTokenId?.trim() !== '') {
      theTokenId = this.patchTokenId;
    }

    if (theTokenId === '' && this.aUser != null && this.aUser !== undefined) {
      await this.aUser.getIdToken(true)
        .then((idToken: any) => {
          theTokenId = idToken;
        });
    }

    if (theTokenId !== '') {
      const aJawaban = confirm('Yakin hapus ?');
      if (aJawaban) {
        const strURL = `${this.myConfig.apiURLdomain}/orm/doc/delete?`;

        this.xSpinner.show();
        const aNewObject = {};
        this.httpClient.post(strURL, aNewObject,
          {
            headers: {
              token_id: theTokenId,
              doc_id: this.theObject.knowledgeID,
              'Content-Type': 'application/json'
            },
            responseType: 'text/plain' as 'json'
          }
        ).subscribe((aaa) => {
          this.xSpinner.hide();
          console.log(aaa);
          this.toastr.success(aaa.toString(), 'Success', {
            timeOut: 1000,
            positionClass: 'toast-bottom-left'
          });
          this.onNew();
        });
      }

    } else {
      const errMessage = 'you have to login or input token id';
      console.log(errMessage);
      this.toastr.error(errMessage, 'Error', {
        timeOut: 1000,
        positionClass: 'toast-bottom-left'
      });
    }
  }

  onTokenChange(agsas: any) {
    localStorage.setItem(enLocStor.googleTokenId, this.patchTokenId);
  }

  onBlur(yeah: string) {
    lastRange = window.getSelection().getRangeAt(0);

    let intFlip = true;
    let theReplacer = '';
    while (yeah.indexOf('```') > -1) {
      if (intFlip)
        theReplacer = '<code>';
      else
        theReplacer = '</code>';
      yeah = yeah.replace('```', theReplacer);

      intFlip = !intFlip;
    }
    this.theObject.content = yeah;
    
  }

  async onSave(divInnerHTML: string, divInnerText: string): Promise<void> {

    let theTokenId = ''
    if (this.patchTokenId?.trim() !== '') {
      theTokenId = this.patchTokenId;
    }

    if (theTokenId === '' && this.aUser != null && this.aUser !== undefined) {
      await this.aUser.getIdToken(true)
        .then((idToken: any) => {
          theTokenId = idToken;
        });
    }

    if (theTokenId !== '') {
      const addParam = environment.production ? '' : '';
      const strURL = `${this.myConfig.apiURLdomain}/orm/doc/save?${addParam}`;
      if (this.theObject.knowledgeID === 'NEW') {
        this.theObject.knowledgeID = Guid.raw().split('-').join('');
      }
      const aNewObject = { ...this.theObject };
      aNewObject.content = divInnerHTML;
      aNewObject.innerTextContent = divInnerText;
      this.xSpinner.show();
      let aHeaders = {
        token_id: theTokenId,
        doc_id: this.theObject.knowledgeID,
        'Content-Type': 'application/json'
      };
      if (aNewObject.publicity === 'password')
      {
        aHeaders['plainPassword'] = this.thePassword
      }
      this.httpClient.post(strURL, aNewObject,
        {
          headers: aHeaders,
          responseType: 'json'
        }
      ).subscribe((aaa: any) => {
        this.xSpinner.hide();
        console.log(aaa);

        this.theObject.content = aaa.newContent;
        this.toastr.success(aaa.caption, 'Success', {
          timeOut: 1000,
          positionClass: 'toast-bottom-left'
        });
      });
    }
    else {
      const errMessage = 'you have to login or input token id';
      console.log(errMessage);
      this.toastr.error(errMessage, 'Error', {
        timeOut: 1000,
        positionClass: 'toast-bottom-left'
      });
    }
  }

  setPassword()
  {
    this.doLoad();
  }

}