import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocMaintainComponent } from './doc-maintain.component';
import { RouterModule, Routes } from '@angular/router';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { SafeHtmlPipeModuleModule } from '../safe-html-pipe-module/safe-html-pipe-module.module';
import { RadioButtonModule } from 'primeng/radiobutton';

const leRoute: Routes = [
  {
    path: '',
    component: DocMaintainComponent
  }
]

@NgModule({
  declarations: [
    DocMaintainComponent,
  ],
  imports: [
    CommonModule,
    InputTextModule,
    FormsModule,
    RadioButtonModule,
    SafeHtmlPipeModuleModule,
    ButtonModule,
    RouterModule.forChild(leRoute)
  ]
})
export class DocMaintainModule { }
