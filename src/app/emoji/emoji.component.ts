import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';
import { redisGetObject } from '../main-page/main-page.component';

@Component({
  selector: 'app-emoji',
  templateUrl: './emoji.component.html',
  styleUrls: ['./emoji.component.css']
})
export class EmojiComponent implements OnInit {
  yeah: HTMLElement = document.querySelector('#theIcon');
  leArray = [];
  theFilter = {
    keyword: ''
  };

  myConfig: xConfiguration;

  myFunction(): any[] {
    this.leArray.forEach((theItem) => {
      const aaa = [theItem.name, theItem.keyword].join('~');
      theItem.visible = (aaa.indexOf(this.theFilter.keyword) > -1)
    })
    const copyan = this.leArray.filter(yeah => yeah.visible === true);
    return copyan;
  }

  constructor(
    private httpClient: HttpClient,
    private toastr: ToastrService
  ) {
    const yeah: any = document.querySelector('#theIcon');
    // console.log(yeah);
    // yeah.href = 'https://emojipedia.org/static/img/favicons/favicon-32x32.png';
    this.doPatch01();
  }

  async doPatch01() {
    redisGetObject(this.httpClient, 'EmojiComponent').then(aObj => {
      this.yeah['href'] = aObj.icon;
      document.title = aObj.title;
    })
  }

  onItemClick(which: any) {
    const what = which.value;
    navigator.clipboard.writeText(what);
    this.toastr.success(`${what} Copied`, 'Success', {
      timeOut: 1000,
      positionClass: 'toast-top-left'
    });
  }

  async ngOnInit(): Promise<void> {
    redisGetObject(this.httpClient, 'emojies').then(theResult => {
      this.leArray = theResult;
      this.leArray.forEach((theItem) => {
        theItem.visible = true;
      })
    })
  }
}
