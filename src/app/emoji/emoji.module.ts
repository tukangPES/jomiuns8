import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EmojiComponent } from './emoji.component';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';

const aRoute: Routes = [
  {path: '', component: EmojiComponent}
];

@NgModule({
  declarations: [EmojiComponent],
  imports: [
    CommonModule,
    InputTextModule,
    ButtonModule,
    FormsModule,
    RouterModule.forChild(aRoute)
  ]
})
export class EmojiModule { }
