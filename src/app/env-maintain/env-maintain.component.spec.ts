import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvMaintainComponent } from './env-maintain.component';

describe('EnvMaintainComponent', () => {
  let component: EnvMaintainComponent;
  let fixture: ComponentFixture<EnvMaintainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnvMaintainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvMaintainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
