import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-env-maintain',
  templateUrl: './env-maintain.component.html',
  styleUrls: ['./env-maintain.component.css']
})
export class EnvMaintainComponent implements OnInit {

  theEnv = {};
  theName = '';

  constructor(private http: HttpClient) { }

  async ngOnInit(): Promise<void> {
    this.theEnv = await readConfig(this.http);
  }
}

export abstract class xConfiguration {
  public apiURLdomain: string
}

export const readConfig = async function (inHttp: HttpClient): Promise<xConfiguration> {
  let someting: any = {};
  await inHttp.get('./assets/something.json').toPromise()
    .then((ahkasg: any) => {
      if (window.location.hostname === 'localhost') {
        ahkasg.apiURLdomain = 'http://api.jomiuns.com';
      } else {
        const yeah = window.location.hostname.split('.');
        ahkasg.apiURLdomain = [ahkasg.apiURLdomain, yeah[yeah.length - 2], yeah[yeah.length - 1]].join('.');
      }
      someting = ahkasg;
    })
  return someting;
}
