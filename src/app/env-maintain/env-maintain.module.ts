import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EnvMaintainComponent } from './env-maintain.component';
import { FormsModule } from '@angular/forms';

const aRoute: Routes = [
  {path: '', component: EnvMaintainComponent}
];

@NgModule({
  declarations: [EnvMaintainComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(aRoute)
  ]
})
export class EnvMaintainModule { }
