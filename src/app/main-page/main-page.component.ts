import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  androidApps: any;
  myConfig: xConfiguration;

  constructor(
    private router: Router,
    private httpClient: HttpClient
  ) { }

  yooooo = '<i>Loading..</i>';

  something = {};

  async ngOnInit(): Promise<void> {
    this.myConfig = await readConfig(this.httpClient);
    this.something = await redisGetObject(this.httpClient, 'robloxGameByLivanAfryanto');
    this.androidApps = await redisGetObject(this.httpClient, 'androidApps');

    const strURL = `${this.myConfig.apiURLdomain}/orm/doc/about-jomiuns?raw=true`;
    const aHasil:any = await this.httpClient.get(strURL).toPromise();
    this.yooooo = aHasil.content;
  }

  onLoginClick() {
    this.router.navigateByUrl('/login');
  }
}

export async function redisGetText(http: HttpClient, instrKey: string) {
  const aConfig = await readConfig(http);
  const strURL = `${aConfig.apiURLdomain}/redisGet?key=${instrKey}`;
  let aHasil;
  await http.get(strURL, { responseType: 'text/plain' as 'json' })
    .toPromise()
    .then((foo: any) => {
      aHasil = foo;
    });
  return aHasil;
}


export async function redisSet(
  http: HttpClient,
  instrKey: string,
  theObject: any,
  instrAuthKey: string,
  inResponseType: 'json' | 'text/plain'): Promise<Object> {
  const aConfig = await readConfig(http);
  const strURL = `${aConfig.apiURLdomain}/redisSet?key=${instrKey}`;

  const theResponse = await http.post(strURL, theObject, {
    headers: {
      token_id: instrAuthKey,
      'Content-Type': 'text/plain'
    }, responseType: inResponseType as 'json'
  }).toPromise();
  return theResponse;
}

export async function redisGetObject(http: HttpClient, instrKey: string): Promise<any> {
  const aConfig = await readConfig(http);
  const strURL = `${aConfig.apiURLdomain}/redisGet?key=${instrKey}`;
  let aHasil;
  await http.get(strURL)
    .toPromise()
    .then((foo: any) => {
      aHasil = foo;
    });
  return aHasil;
}