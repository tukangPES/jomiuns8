import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { MainPageComponent } from './main-page.component';
import { CarouselModule } from 'primeng/carousel';
import { SafeHtmlPipeModuleModule } from '../safe-html-pipe-module/safe-html-pipe-module.module';

const aRoute: Routes = [
  { path: '', component: MainPageComponent }
];

@NgModule({
  declarations: [
    MainPageComponent
    ],
  imports: [
    CommonModule,
    ButtonModule,
    SafeHtmlPipeModuleModule,
    CardModule,
    CarouselModule,
    RouterModule.forChild(aRoute)
  ]
})
export class MainPageModule { }
