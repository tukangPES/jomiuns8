import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapostComponent } from './mapost.component';

describe('MapostComponent', () => {
  let component: MapostComponent;
  let fixture: ComponentFixture<MapostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
