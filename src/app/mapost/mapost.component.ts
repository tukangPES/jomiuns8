import { json } from '@angular-devkit/core';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { threadId } from 'worker_threads';

@Component({
  selector: 'app-mapost',
  templateUrl: './mapost.component.html',
  styleUrls: ['./mapost.component.css']
})
export class MapostComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }
  methods = [{ caption: 'POST', value: 'POST' }];
  leConfig = {
    URL: '',
    bodyAsString: '',
    theObject: {}
  };

  ngOnInit(): void {

  }

  onChange01(theEvent)
  {
    this.leConfig.theObject = JSON.parse(this.leConfig.bodyAsString);
    console.log('aaa');
    
  }

  async doSend() {
    const theResult = await this.httpClient.post(this.leConfig.URL, this.leConfig.theObject).toPromise();
    console.log('theResult', theResult);
  }

}
