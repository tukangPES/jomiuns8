import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapostComponent } from './mapost.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';

const aRoute: Routes = [
  {path: '', component: MapostComponent}
];

@NgModule({
  declarations: [MapostComponent],
  imports: [
    CommonModule,
    FormsModule,
    InputTextModule,
    ButtonModule,
    DropdownModule,
    RouterModule.forChild(aRoute)
  ]
})

export class MapostModule { }

