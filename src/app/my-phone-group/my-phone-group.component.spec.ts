import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPhoneGroupComponent } from './my-phone-group.component';

describe('MyPhoneGroupComponent', () => {
  let component: MyPhoneGroupComponent;
  let fixture: ComponentFixture<MyPhoneGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyPhoneGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPhoneGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
