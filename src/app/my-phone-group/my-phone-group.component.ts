import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { whaPhone, whaPhoneGroup } from 'src/classes/interfaces';

@Component({
  selector: 'app-my-phone-group',
  templateUrl: './my-phone-group.component.html',
  styleUrls: ['./my-phone-group.component.css']
})
export class MyPhoneGroupComponent implements OnInit {

  @Input() phoneGroup: whaPhoneGroup | undefined = undefined;
  @Input() phones:whaPhone[] = [];
  @Input() lang = '';
  @Input() functionToCall: any = {};
  groupedPhone: whaPhone[]= [];

  constructor() { }

  ngOnInit(): void {
    this.groupedPhone = this.phones.filter(thePhone => {
      if (this.phoneGroup === undefined)
      {
        return false;
      } else {
        const anAngka: number = (thePhone.flagId & this.phoneGroup.flagId);
        return (anAngka !== 0);
      }
    });
  }

}
