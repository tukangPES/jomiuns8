import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedisMaintainComponent } from './redis-maintain.component';

describe('RedisMaintainComponent', () => {
  let component: RedisMaintainComponent;
  let fixture: ComponentFixture<RedisMaintainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedisMaintainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RedisMaintainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
