import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { enLocStor } from 'src/classes/enumss';
import { environment } from 'src/environments/environment';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';
import { redisGetObject, redisGetText, redisSet } from '../main-page/main-page.component';

@Component({
  selector: 'app-redis-maintain',
  templateUrl: './redis-maintain.component.html',
  styleUrls: ['./redis-maintain.component.css']
})
export class RedisMaintainComponent implements OnInit {

  theParam: any = {
    value: ''
  };
  theResult: any = [];
  isMuncul = false;

  harusMoving: any = {};
  currentKey: string;
  isAddingNewKey: boolean;
  titik01: any;
  yeah: HTMLElement = document.querySelector('#theIcon');
  lastSelected: HTMLElement = null;
  myConfig: xConfiguration;

  constructor(
    private toastr: ToastrService,
    private xSpinner: NgxSpinnerService,
    private httpClient: HttpClient) {

  }

  async doPatch01() {
    redisGetObject(this.httpClient, 'RedisMaintainComponent').then(aObj => {
      this.yeah['href'] = aObj.icon;
      document.title = aObj.title;
    })
  }

  doSaveValue() {
    this.xSpinner.show();

    redisSet(this.httpClient, this.currentKey, this.theParam.value, this.theParam.authKey, 'text/plain').then(theResult => {
      console.log(theResult);
      
      this.toastr.success(`${theResult}`, '', {
        timeOut: 1000,
        positionClass: 'toast-top-left'
      });
    }).catch((theError) => {
      console.log(theError);
    })
      .finally(() => {
        this.xSpinner.hide();
      })
  }

  doHide() {
    this.isMuncul = false;
    this.isAddingNewKey = false;
  }

  doAddNew() {
    this.isMuncul = true;
    this.isAddingNewKey = true;
    this.titik01.after(this.harusMoving);
  }

  async doMaintain(which: any) {
    this.isAddingNewKey = false;
    this.isMuncul = true;
    const aaa: HTMLElement = which.srcElement;
    this.currentKey = aaa.innerText.replace("\n", '');
    redisGetText(this.httpClient, this.currentKey).then(result => {
      this.theParam.value = result;
    })

    if (this.lastSelected != null) {
      this.lastSelected.classList.remove('selectedKey');
    }
    this.lastSelected = aaa;
    aaa.classList.add('selectedKey');

    aaa.after(this.harusMoving);
  }

  async ngOnInit(): Promise<void> {
    this.myConfig = await readConfig(this.httpClient);
    this.doPatch01();
    this.theParam.authKey = localStorage.getItem(enLocStor.xToken) ?? '';
    this.harusMoving = document.querySelector('#yeahSomething');
    this.titik01 = document.querySelector('#yeah01');
    console.log('harusMoving', this.harusMoving);
  }

  saveToken() {
    if (this.theParam.authKey !== '') {
      localStorage.setItem(enLocStor.xToken, this.theParam.authKey);
    }
  }

  doCari() {
    this.saveToken();
    const strURL = `${this.myConfig.apiURLdomain}/keys?key=${this.theParam.keyPattern}`;
    this.xSpinner.show();
    this.httpClient.get(strURL,
      {
        headers: {
          token_id: this.theParam.authKey
        },
        responseType: 'application/json' as 'json'
      }
    )
      .toPromise()
      .then((aaa: any) => {
        this.theResult = JSON.parse(aaa);
        this.xSpinner.hide();
      })
      .finally(() => {
        this.xSpinner.hide();
      });
  }

}
