import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RedisMaintainComponent } from './redis-maintain.component';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';

const theRoute: Routes = [
  {
    path: '',
    component: RedisMaintainComponent
  }
]

@NgModule({
  declarations: [RedisMaintainComponent],
  imports: [
    CommonModule,
    FormsModule,
    InputTextModule,
    ButtonModule,
    RouterModule.forChild(theRoute)
  ]
})
export class RedisMaintainModule { }
