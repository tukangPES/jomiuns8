import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeHtmlPipe } from '../SafeHtmlPipe';

@NgModule({
  declarations: [
    SafeHtmlPipe
  ],
  imports: [
    CommonModule
  ],
  exports:[
    SafeHtmlPipe
  ]
})
export class SafeHtmlPipeModuleModule { }
