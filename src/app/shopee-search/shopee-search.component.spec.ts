import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopeeSearchComponent } from './shopee-search.component';

describe('ShopeeSearchComponent', () => {
  let component: ShopeeSearchComponent;
  let fixture: ComponentFixture<ShopeeSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopeeSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopeeSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
