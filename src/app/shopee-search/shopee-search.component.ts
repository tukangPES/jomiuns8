import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { shopeeLocation, shopeePromo, xShopeeSelection } from 'src/classes/interfaces';
import { ButtonChangerComponent } from '../button-changer/button-changer.component';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';
import { redisGetObject } from '../main-page/main-page.component';

@Component({
  selector: 'app-shopee-search',
  templateUrl: './shopee-search.component.html',
  styleUrls: ['./shopee-search.component.css']
})

export class ShopeeSearchComponent implements OnInit {
  yeah: HTMLElement = document.querySelector('#theIcon');
  theLocations: shopeeLocation[] = [];
  thePromos: shopeePromo[] = [];
  lastQuery = '';
  theSelection: xShopeeSelection = {
    locations: [],
    promos: [],
    query: '',
    orderBy: null,
    maxPrice: undefined,
    minPrice: undefined
  };

  theOrder = [
    { caption: '-pilih-', value: null },
    { caption: 'Harga Terendah', value: 'price' }
  ];

  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient
  ) {

    this.doPatch01();

  }

  myConfig: xConfiguration;

  async doPatch01() {
    redisGetObject(this.httpClient, 'ShopeeSearchComponent').then(aObj => {
      this.yeah['href'] = aObj.icon;
      document.title = aObj.title;
    })
  }

  async ngOnInit(): Promise<void> {
    this.myConfig = await readConfig(this.httpClient);
    const theStringified = this.cookieService.get('yourShopeeFilter');
    if (theStringified != null &&
      theStringified !== undefined &&
      theStringified !== '') {

      this.theSelection = JSON.parse(theStringified);
      if (this.theSelection.locations === undefined) {
        this.theSelection.locations = [];
      }
      if (this.theSelection.promos === undefined) {
        this.theSelection.promos = [];
      }
    }

    redisGetObject(this.httpClient, 'shopeeParam').then(shoPar => {
      if (shoPar === undefined) {
        alert('gagal ambil shopee parameter');
        return;
      }
      this.theLocations = shoPar.shopeeLocations;
      this.thePromos = shoPar.shopeePromo;
    })
  }

  saveFilter(): void {
    const stringified = JSON.stringify(this.theSelection);
    this.cookieService.set('yourShopeeFilter', stringified);
  }

  onButtonClicked(which: ButtonChangerComponent) {

    let ooooo = [
      { xxx: 'freeShipping', yyy: '1000006' },
      { xxx: '1000006', yyy: 'freeShipping' }
    ]

    ooooo.forEach(hehe => {
      if (which.value == hehe.xxx && which.xChecked == true) {
        //keluarkan 1000006 dari daftar pilihan
        const theIndexTemp = this.theSelection.promos.indexOf(hehe.yyy);
        if (theIndexTemp >= 0) {
          this.theSelection.promos.splice(theIndexTemp, 1);
        }

        //uncheck si 1000006
        const theHasil = this.thePromos.find(theYeah => theYeah.promoId === hehe.yyy);
        if (theHasil !== undefined) {
          theHasil.theObject.xChecked = false;
        }
      }

    })
  }

  onItemCreated(hehehee: ButtonChangerComponent, iiii: number) {
    this.thePromos[iiii].theObject = hehehee;
  }

  doSearch(): void {
    this.saveFilter();

    const encodedKeyword = encodeURIComponent(this.theSelection.query);
    let params = [];
    params.push(`keyword=${encodedKeyword}`);
    if (this.theSelection.locations.length > 0) {
      const locations = encodeURIComponent(this.theSelection.locations.join(','));
      params.push(`locations=${locations}`)
    }

    const hahaha = this.theSelection.promos as string[];
    if (hahaha.indexOf('freeShipping') >= 0) {
      params.push('freeShipping=true');
      this.theSelection.promos = this.theSelection.promos.filter((yeah: string) => yeah !== 'freeShipping');
    }

    if (this.theSelection.orderBy != null) {
      params.push(`sortBy=${this.theSelection.orderBy.value}`);
      params.push('order=asc');
    }

    if (this.theSelection.maxPrice !== undefined) {
      params.push(`maxPrice=${this.theSelection.maxPrice}`);
    }

    if (this.theSelection.minPrice !== undefined) {
      params.push(`minPrice=${this.theSelection.minPrice}`);
    }

    if (this.theSelection.promos.length > 0) {
      const labelIds = encodeURIComponent(this.theSelection.promos.join(','));
      params.push(`labelIds=${labelIds}`)
    }

    const paramAsString = params.join('&');
    let strURL = `https://shopee.co.id/search?${paramAsString}`;
    window.open(strURL);
  }
}