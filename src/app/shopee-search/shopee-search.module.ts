import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonChangerModule } from '../button-changer/button-changer.module';
import { ShopeeSearchComponent } from './shopee-search.component';


const theRoute: Routes = [
  {
    path: '',
    component: ShopeeSearchComponent
  }
]

@NgModule({
  declarations: [ShopeeSearchComponent],
  imports: [
    CommonModule,
    FormsModule,
    ButtonChangerModule,
    InputTextModule,
    ButtonModule,
    CheckboxModule,
    DropdownModule,
    RouterModule.forChild(theRoute)
  ]
})
export class ShopeeSearchModule { }
