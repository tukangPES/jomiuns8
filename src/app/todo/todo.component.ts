import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Guid } from 'guid-typescript';
import { NgxSpinnerService } from 'ngx-spinner';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  theYeah = [];
  theFoo = {} as any;
  myConfig: xConfiguration;

  constructor(private _httpClient: HttpClient,
    private xSpinner: NgxSpinnerService,
  ) { }

  async onNew() {
    this.xSpinner.show();
    var strURL = `${this.myConfig.apiURLdomain}/orm/todo`;
    const yeahObject = {
      title: this.theFoo.newTask,
      todoID: Guid.create().toString()
    };
    await this._httpClient.post(strURL, yeahObject, {
      responseType: 'text/plain' as 'json'
    }).toPromise();

    var aTest: any[] = await this._httpClient.get(strURL).toPromise() as [];
    this.theYeah = [...aTest];
    this.doSort();
    this.xSpinner.hide();
  }

  async ngOnInit(): Promise<void> {

    this.xSpinner.show();
    this.myConfig = await readConfig(this._httpClient);
    var strURL = `${this.myConfig.apiURLdomain}/orm/todo`;
    var aTest: any[] = await this._httpClient.get(strURL).toPromise() as [];
    this.theYeah = [...aTest];
    this.doSort();
    this.xSpinner.hide();
  }

  doSort() {
    this.theYeah.sort((ooo: any, bbb: any) => {
      if (ooo.isDone) return 1
      else if (ooo.isDone == false) return -1
      else return 0
    });
  }

  async klhasglhkas(theEvent: any, theObject: any) {
    console.log(theObject);
    var strURL = `${this.myConfig.apiURLdomain}/orm/todo`;
    await this._httpClient.post(strURL, theObject, {
      responseType: 'text/plain' as 'json'
    }).toPromise();
    this.doSort();
  }

  async onDelete(theObject: any) {
    console.log(theObject);
    var strURL = `${this.myConfig.apiURLdomain}/orm/todo/delete`;
    this.xSpinner.show();
    await this._httpClient.post(strURL, theObject,
      {
        responseType: 'text/plain' as 'json'
      }).toPromise();

    strURL = `${this.myConfig.apiURLdomain}/orm/todo`;
    var aTest: any[] = await this._httpClient.get(strURL).toPromise() as [];
    this.theYeah = [...aTest];
    this.doSort();
    this.xSpinner.hide();
  }

}
