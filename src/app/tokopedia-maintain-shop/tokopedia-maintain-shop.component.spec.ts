import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokopediaMaintainShopComponent } from './tokopedia-maintain-shop.component';

describe('TokopediaMaintainShopComponent', () => {
  let component: TokopediaMaintainShopComponent;
  let fixture: ComponentFixture<TokopediaMaintainShopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TokopediaMaintainShopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TokopediaMaintainShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
