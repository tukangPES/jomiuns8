import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { enLocStor } from 'src/classes/enumss';
import { environment } from 'src/environments/environment';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';
import { redisGetObject, redisSet } from '../main-page/main-page.component';

@Component({
  selector: 'app-tokopedia-maintain-shop',
  templateUrl: './tokopedia-maintain-shop.component.html',
  styleUrls: ['./tokopedia-maintain-shop.component.css']
})
export class TokopediaMaintainShopComponent implements OnInit {
  courierToMaintain: any[];
  myConfig: xConfiguration;
  selectedCourier: any;

  @Input() something: any = {};
  @Input() locations: any[];
  xLocation: string = '';
  xPrice: number = 0;
  xToken: string = '';
  @Output() onRequestHide: EventEmitter<void>;

  constructor(
    private xSpinner: NgxSpinnerService,
    private http: HttpClient) {

    this.courierToMaintain = [];
    this.onRequestHide = new EventEmitter();
  }

  onHide() {
    this.onRequestHide?.emit();
  }

  saveToken() {
    if (this.xToken !== '') {
      localStorage.setItem(enLocStor.xToken, this.xToken);
    }
  }

  get ribuPrice(): number {
    return this.xPrice / 1000;
  }
  set ribuPrice(value: number) {
    this.xPrice = value * 1000;
  }

  async ngOnInit(): Promise<void> {
    this.myConfig = await readConfig(this.http);
    const lastListCourier = localStorage.getItem(enLocStor.couriersToMaintain);
    if (lastListCourier == null) {
      redisGetObject(this.http, 'couriers').then(hasil => {
        if (hasil != null) {
          localStorage.setItem(enLocStor.couriersToMaintain, JSON.stringify(hasil));
          this.courierToMaintain = hasil;
          if (this.courierToMaintain !== undefined && this.courierToMaintain.length > 0) {
            this.selectedCourier = this.courierToMaintain[0];
          }
        }
      })

    } else {
      this.courierToMaintain = JSON.parse(lastListCourier);
      if (this.courierToMaintain !== undefined && this.courierToMaintain.length > 0) {
        this.selectedCourier = this.courierToMaintain[0];
      }
    }

    this.xLocation = localStorage.getItem(enLocStor.lastPlaceToMaintain);
    this.xToken = localStorage.getItem(enLocStor.xToken) ?? '';
    const theShopId = this.something.id ?? '';

    redisGetObject(this.http, `tokopedia.toko.${theShopId}`).then(hasil => {
      Object.assign(this.something, hasil);
      this.onTitikAntarChange(this.xLocation);
    })
  }

  onSetGoSend() {
    this.saveToken();
    const theKey = this.selectedCourier.theKey;

    if (this.xLocation !== '') {
      if (this.something[theKey] === undefined) {
        this.something[theKey] = {};
      }
      const xRef = this.something[theKey];
      xRef[this.xLocation] = this.xPrice;
      this.saveObject();

      localStorage.setItem(enLocStor.lastPlaceToMaintain, this.xLocation);
    }
  }

  saveObject() {
    const theShopId = this.something.id ?? '';
    if (theShopId !== '' && this.xToken !== '') {
      this.xSpinner.show();
      redisSet(this.http, `tokopedia.toko.${theShopId}`, this.something, this.xToken, 'text/plain')
        .then((theResult) => {
          console.log(theResult);
          this.onRequestHide?.emit();
        })
        .catch((theError) => {
          //this.onRequestHide?.emit();
          console.log(theError);
        })
        .finally(() => {
          this.xSpinner.hide();
        })
    }
  }

  onTitikAntarChange(titikAntarKey) {
    const keyKurir = this.selectedCourier.theKey;
    const theKurir = this.something[keyKurir];
    const theBiaya = theKurir[titikAntarKey];
    this.xPrice = theBiaya;
  }

  onRemoveLocation() {
    this.saveToken();
    if (this.xLocation !== '') {
      if (this.something.gosend === undefined) {
        this.something.gosend = {};
      }
      delete this.something.gosend[this.xLocation];
      this.saveObject();
    }
  }

}