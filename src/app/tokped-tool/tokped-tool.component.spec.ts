import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokpedToolComponent } from './tokped-tool.component';

describe('TokpedToolComponent', () => {
  let component: TokpedToolComponent;
  let fixture: ComponentFixture<TokpedToolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TokpedToolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TokpedToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
