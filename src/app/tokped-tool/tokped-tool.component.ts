import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { IContextMenuClickEvent } from 'ngx-contextmenu';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { captionAndValue, tokpedFilter, tokpedResultItem, tokpedShop } from 'src/classes/interfaces';
import { environment } from 'src/environments/environment';
import { enLocStor } from 'src/classes/enumss';
import { readConfig, xConfiguration } from '../env-maintain/env-maintain.component';
import { redisGetObject } from '../main-page/main-page.component';

@Component({
  selector: 'app-tokped-tool',
  templateUrl: './tokped-tool.component.html',
  styleUrls: ['./tokped-tool.component.css']
})

export class TokpedToolComponent implements OnInit {
  myParams = {
    locations: [] as any[],
    ukuranGambar: [] as any[],
  };

  titikTujuanAntar = [
    { label: 'Jomiuns', value: 'Jomiuns' },
    { label: 'WarnetMagnet', value: 'WarnetMagnet' },
    { label: 'Arwana5', value: 'Arwana5' }
  ];

  selectedText: string | undefined;
  lastItem: tokpedResultItem | undefined;
  theResultsNya: tokpedResultItem[] | undefined;
  lastQuery = '';
  selectedShop: tokpedShop | any = {};
  dctShopIdVsShop: {
    [xkey: string]: tokpedShop;
  } = {}; //dictionary
  showTheComponent = false;

  doClearCache() {
    localStorage.removeItem(enLocStor.lastTokpedResultData);
    this.toastr.success('Result Data Cleared', 'Success', {
      timeOut: 1000,
      positionClass: 'toast-top-left'
    });

    this.theResultsNya = [];
    this.lastQuery = '';
  }

  theSelection: tokpedFilter | any = {
    shipping: [],
    condition: [],
    hargaPer: 1,
    filterCity: [],
    excludeKeyword: '',
    rows: 500,
    theImgClass: 'img100',
    powerMerchant: false,
    officialMerchant: false
  };

  constructor(
    private cookieService: CookieService,
    private http: HttpClient,
    private xSpinner: NgxSpinnerService,
    private toastr: ToastrService) {

    const yeah: any = document.querySelector('#theIcon');
    yeah.href = 'https://ecs7.tokopedia.net/assets-tokopedia-lite/prod/icon512.png';
    document.title = 'Tokped Helper';

  }

  theOrder: captionAndValue[] = [
    { caption: '-pilih-', value: 0 },
    { caption: 'Harga Terendah', value: 3 }
  ];

  myConfig: xConfiguration;

  async ngOnInit(): Promise<void> {
    this.myConfig = await readConfig(this.http);
    let theStringified = this.cookieService.get('yourFilter');

    if (theStringified != null && theStringified !== undefined && theStringified !== '') {
      this.theSelection = JSON.parse(theStringified);
      
    }

    let hoooo = localStorage.getItem(enLocStor.lastTokpedResultData);
    if (hoooo != null && hoooo !== undefined && hoooo !== '') {
      const lastResultNya = JSON.parse(hoooo);
      if (lastResultNya != null && lastResultNya !== undefined) {
        this.theResultsNya = lastResultNya;
      }
    }

    this.doGetTokopediaParam();

  }

  doGetTokopediaParam() {
    redisGetObject(this.http, 'tokopediaParam').then(theResult => {
      this.myParams = theResult;
    })
  }

  onEnter(theEvent:any){
    this.doSearch();
  }

  clearTextBox(prop: string): void {
    this.theSelection[prop] = '';
  }

  onItemClick(theItem: tokpedResultItem): void {
    if (this.lastItem != null) {
      this.lastItem.selected = false;
    }
    theItem.selected = true;
    this.lastItem = theItem;
    window.open(theItem.url);
  }

  saveFilter(): void {
    const stringified = JSON.stringify(this.theSelection);
    this.cookieService.set('yourFilter', stringified);
  }

  async doSearch(): Promise<void> {
    this.saveFilter();

    const oTemp01 = { ...this.theSelection };
    let sama = false;
    let beda = false;

    oTemp01.excludeKeyword = null;
    oTemp01.theImgClass = null;

    const jsonString = JSON.stringify(oTemp01);
    if (this.lastQuery === jsonString) {
      sama = true;
    } else {
      beda = true;
      this.lastQuery = jsonString;
    }

    if (beda) {
      this.theResultsNya = await this.fetchDataFromServer();
      if (this.theResultsNya != null) {
        this.excludeResult(this.theResultsNya);
      }
    }

    if (sama) {
      if (this.theResultsNya != null) {
        this.excludeResult(this.theResultsNya);
      }
    }

    // simpulkan shop2 
    this.theResultsNya?.forEach(theItem => {
      const shopId = theItem.shop.id ?? '';
      var asglha = this.dctShopIdVsShop[shopId];
      if (asglha == null) {
        this.dctShopIdVsShop[shopId] = theItem.shop;
      } else {
        theItem.shop = asglha;
      }
    });

    const aaaa = Object.keys(this.dctShopIdVsShop).values();
    const bla = Array.from(aaaa);
    const bli = bla.map((theOOO) => {
      return `tokopedia.toko.${theOOO}`
    })
    console.log(bli);

    const strURL = `${this.myConfig.apiURLdomain}/redisMGet`;
    this.http.post(strURL, bli).toPromise()
      .then(aResult => {
        console.log(aResult);

        let aInt = 0;
        bla.map(theKey => {
          console.log(theKey, aResult[aInt]);
          const aInfo = aResult[aInt];
          if (aInfo != null && aInfo !== undefined && aInfo !== '') {
            const aObj = JSON.parse(aResult[aInt]);
            Object.assign((this.dctShopIdVsShop as any)[theKey], aObj);
            //this.dctShopIdVsShop[theKey] = aResult[aInt];
          }
          aInt++;
        })
      })

    //sort ah
    this.doSort();

    const stringified = JSON.stringify(this.theResultsNya);
    localStorage.setItem(enLocStor.lastTokpedResultData, stringified);

    const yangVisible = this.theResultsNya.filter(theItem => theItem.visible === true);
    const strText = `${yangVisible.length} items`;
    this.toastr.success(strText, 'Total Items', {
      timeOut: 1000,
      positionClass: 'toast-top-left'
    });

  }

  excludeResult(anArrayOfDagangan: tokpedResultItem[]): void {
    const daTokpedSelection = this.theSelection as tokpedFilter;
    if (daTokpedSelection.excludeKeyword != null &&
      daTokpedSelection.excludeKeyword !== '') {

      const oExcludes = daTokpedSelection.excludeKeyword.split(',').map((item: string) => `\\b${item.trim()}\\b`);
      const theString = oExcludes.join('|');
      const theExcludePatternString = `(${theString})`;
      const regExPattern = new RegExp(theExcludePatternString, 'i');
      anArrayOfDagangan.forEach((theItem: tokpedResultItem) => {
        const strToCompare = [
          theItem.name,
          theItem.shop.name,
          theItem.shop.city].join(' ');
        theItem.visible = !strToCompare.match(regExPattern);
      });
    }
    else {
      anArrayOfDagangan.forEach(theItem => {
        theItem.visible = true;
      });
    }
  }

  doSort() {
    this.theResultsNya.sort((item1, item2) => {
      if (item1.price > item2.price) {
        return 1;
      } else if (item1.price < item2.price) {
        return -1;
      } else {
        return 0;
      }
    })
  }

  excludeThis(theItem: tokpedResultItem, propertyName: string): void {
    this.saveFilter();
    const theYeah = (theItem.shop as any)[propertyName];
    const daTokpedSelection = this.theSelection as tokpedFilter;
    const theSomething =
      daTokpedSelection.excludeKeyword
        .split(',')
        .filter(Boolean)
        .map(theItem => theItem.trim());

    daTokpedSelection.excludeKeyword = theSomething.concat(theYeah).join(', ');

    if (this.theResultsNya != null) {
      this.excludeResult(this.theResultsNya);
    }
  }

  manageShop(theInfo: any) {
    this.showTheComponent = false;
    this.selectedShop = theInfo;
    this.showTheComponent = true;
  }

  closeTheComponent() {
    this.showTheComponent = false;
  }

  async fetchDataFromServer(): Promise<tokpedResultItem[] | undefined> {
    const strURL = `${this.myConfig.apiURLdomain}/proj01/tokpedSearch`;
    let aSomething: tokpedResultItem[] | undefined;
    const yoyoyo = this.http.post(strURL, this.theSelection).toPromise();
    this.xSpinner.show();
    await yoyoyo.then(result => {
      aSomething = result as tokpedResultItem[];
    });
    this.xSpinner.hide();
    console.log(aSomething);

    return aSomething;
  }

  onExcludeFromMenu() {
    const yeaaaa = window.getSelection()?.toString();
    const daTokpedSelection = this.theSelection as tokpedFilter;
    const theSomething =
      daTokpedSelection.excludeKeyword
        .split(',')
        .filter(Boolean)
        .map(theItem => theItem.trim());

    if (yeaaaa != undefined) {
      theSomething.push(yeaaaa);
      daTokpedSelection.excludeKeyword = theSomething.join(', ');
    }

    this.doSearch();
  }

  hideThis(which: tokpedResultItem) {
    which.visible = false;
  }

  onMenuOpen(hehehe: IContextMenuClickEvent) {
    console.log('on context menu');
    const yeaaaa = window.getSelection()?.toString();
    this.selectedText = yeaaaa;
  }

  cobaCoba(theShop: any) {
    let coba = { ...theShop };
    delete coba.id;
    delete coba.name;
    delete coba.city;
    delete coba.url;
    delete coba.isOfficial;
    delete coba.isPowerBadge;
    if (JSON.stringify(coba) === '{}') {
      return null;
    }
    return coba;
  }

}
