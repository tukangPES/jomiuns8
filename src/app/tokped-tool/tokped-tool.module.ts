import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ToastrModule } from 'ngx-toastr';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ButtonChangerModule } from '../button-changer/button-changer.module';
import { TokopediaMaintainShopComponent } from '../tokopedia-maintain-shop/tokopedia-maintain-shop.component';
import { TokpedToolComponent } from './tokped-tool.component';

const rout: Routes = [
  { path: '', component: TokpedToolComponent }
]

@NgModule({
  declarations: [
    TokpedToolComponent,
    TokopediaMaintainShopComponent,
  ],
  imports: [
    ButtonChangerModule,
    CommonModule,
    ContextMenuModule,
    CardModule,
    SelectButtonModule,
    ButtonModule,
    DropdownModule,
    FormsModule,
    InputTextModule,
    ToastrModule,
    ToggleButtonModule,
    CheckboxModule,
    RouterModule.forChild(rout)
  ]
})
export class TokpedHelperModule { }
