import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XloginComponent } from './xlogin.component';

describe('XloginComponent', () => {
  let component: XloginComponent;
  let fixture: ComponentFixture<XloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
