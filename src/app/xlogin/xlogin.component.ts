import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import firebase from 'firebase/app';
import 'firebase/auth';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-xlogin',
  templateUrl: './xlogin.component.html',
  styleUrls: ['./xlogin.component.css']
})
export class XloginComponent implements OnInit {
  firebaseConfig = {
    apiKey: 'AIzaSyD08Wpbz2EnlsGbwMVYGamgBvnhhdZCOZo',
    authDomain: 'api-8234549410433384822-425177.firebaseapp.com',
    databaseURL: 'https://api-8234549410433384822-425177-default-rtdb.asia-southeast1.firebasedatabase.app/',
    storageBucket: 'api-8234549410433384822-425177.appspot.com'
  };

  oUser: firebase.User;
  isLoggedOut = false;

  constructor(
    private actRoute: ActivatedRoute,
    private toastr: ToastrService) {
    this.oUser = undefined;
    firebase.initializeApp(this.firebaseConfig);
  }

  ngOnInit(): void {

    this.actRoute.data.subscribe((routeData) => {
      console.log(routeData);
      if (routeData.isLogOut) {
        console.log('logging out');
        firebase.auth().signOut().then(() => {
          console.log('logged out');
          this.isLoggedOut = true;
          return;
          // Sign-out successful.
        }).catch((error) => {
          // An error happened.
        });

      }
      else {

        firebase.auth().onAuthStateChanged(user => {
          console.log('on auth state changed');
          console.log(user);
          this.oUser = firebase.auth().currentUser;
          if (this.oUser == null) {
            console.log('user is null');
            //const provider = new firebase.auth.GoogleAuthProvider();
            var googleAuthProvider = new firebase.auth.GoogleAuthProvider();
            googleAuthProvider.setCustomParameters({
              prompt: 'select_account'
            });
            firebase.auth().signInWithRedirect(googleAuthProvider);
          }
          else {
            console.log(this.oUser);
          }
        });
      }
    });

    /*
    firebase.auth().getRedirectResult().then(function(result) {
      if (result.credential) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const token = (result.credential as any).accessToken;
        // ...
      }
      // The signed-in user info.
      const user = result.user;
      console.log(user);
    }).catch(function(error) {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // The email of the user's account used.
    o  const email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      const credential = error.credential;
      // ...

      console.log(error);
    });
   */
  }

  onGetTokenId() {
    navigator.clipboard.writeText((this.oUser as any).Aa);
    this.toastr.success('Copied', 'Success', {
      timeOut: 1000,
      positionClass: 'toast-top-left'
    });
  }

}
