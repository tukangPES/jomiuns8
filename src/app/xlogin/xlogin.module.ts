import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { XloginComponent } from './xlogin.component';
import { RouterModule, Routes } from '@angular/router';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ToastrModule } from 'ngx-toastr';


const aaaRoute: Routes = [
  {
    path: '',
    component: XloginComponent
  }
]

@NgModule({
  declarations: [XloginComponent],
  imports: [
    CommonModule,
    CardModule,
    ButtonModule,
    ToastrModule,
    RouterModule.forChild(aaaRoute)
  ]
})
export class XloginModule { }
