export interface tokpedFilter {
    excludeKeyword: string
}

export interface xShopeeSelection {
    promos: string[],
    locations: string[],
    orderBy: any,
    query: string,
    maxPrice: number|undefined,
    minPrice: number|undefined,
}

export interface whaFeaturedPhone {
    phoneGroups: whaPhoneGroup[],
    phoneNumbers: whaPhone[]
}

export interface whaPhone {
    isOfficial: boolean,
    caption: string,
    phone: string,
    flagId: number
}

export interface whaPhoneGroup {
    id: string,
    en: string,
    flagId: number
}

export interface shopeeLocation {
    caption: string
}

export interface shopeePromo {
    caption: string,
    promoId: string
    theObject: any
}

export interface shopeeParameter {
    shopeeLocations: shopeeLocation[],
    shopeePromo: shopeePromo[]
}

export interface tokpedResultItem {
    visible: boolean,
    selected: boolean,
    url: string,
    name: string,
    shop: tokpedShop,
    price: number,
    imageUrl: string
}

export interface tokpedShop {
    id: string
    name: string,
    city: string,
    gosend: any,
    siCepat: any
}

export interface captionAndValue {
    caption: string,
    value: any
}